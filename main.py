# Tip calculator

print("Welcome to the tip calculator")
bill = input("What was the total bill? $")
tipprc = input("What percentage tip would you like to give? 10, 12 or 15? ")
people = input("How many people to split the bill? ")

tip = (float(tipprc) / 100) * float(bill)
total = float(bill) + tip
each = round(total / int(people), 2)

print("Each person should pay: ${:.2f}".format(each))