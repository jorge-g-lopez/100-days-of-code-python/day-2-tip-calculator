# Add the digits of a two-digit number

num = input("Type a two digit number: ")

# input returns a string, no need to convert it

print(int(num[0]) + int(num[1]))