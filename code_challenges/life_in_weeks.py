# Life in weeks

age = input("What is your current age? ")

# The original challenge sets maxium life up to 90 years
# However, I think I'll live something like 85 years

limit = 85
yearsleft = limit - int(age)
monthsleft = yearsleft * 12
weeksleft = yearsleft * 52
daysleft = yearsleft * 365

print(f"You have {daysleft} days, {weeksleft} weeks, and {monthsleft} months left.")